const generateSafeId = require('generate-safe-id');
const config = require('config');
const commonSvc = require('../service/common.service');
const keys = require('../lib/keys');
const { jwtGenerate } = require('../lib/util');

const getUUID = () => Promise.resolve(generateSafeId());

const generateJwtCommnon = commonSvc.jwt(jwtGenerate)(keys.privateKey)(
  config.get('algorithmJWT')
);

const generateJWT = data => Promise.resolve(generateJwtCommnon(data));

module.exports = { getUUID, generateJWT };
