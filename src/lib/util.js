const R = require('ramda');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const config = require('config');

const valuesIndex = data =>
  Object.keys(data).reduce(
    R.curry((sum, _, currentIndex) =>
      currentIndex === 0
        ? `${sum}$${(currentIndex + 1).toString()}`
        : `${sum},$${(currentIndex + 1).toString()}`
    ),
    ''
  );

const getStringKeys = data => Object.keys(data).join(',');

const enCode = (pubKey, content) =>
  crypto.publicEncrypt(pubKey, Buffer.from(content)).toString('base64');

const deCode = (priKey, hashContent) =>
  crypto
    .privateDecrypt(priKey, Buffer.from(hashContent, 'base64'))
    .toString('utf8');

const jwtGenerate = (priKey, algorithm, payload) =>
  jwt.sign(
    { ...payload, exp: config.get('jwtExpTime'), iss: config.get('iss') },
    priKey,
    algorithm
  );

const fConditions = conditions =>
  Object.keys(conditions).reduce(
    (cString, currentC, currentIndex) =>
      currentIndex === 0
        ? `${cString}${currentC} = $${(currentIndex + 1).toString()}`
        : `${cString} AND ${currentC} = $${(currentIndex + 1).toString()}`,
    ''
  );

  
const throwError = error => {
  throw error;
};

module.exports = {
  throwError,
  fConditions,
  jwtGenerate,
  enCode,
  deCode,
  valuesIndex,
  getStringKeys,
};
