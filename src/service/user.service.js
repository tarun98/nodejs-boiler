const commonSvc = require('./common.service');

const {
  fConditions,
  getStringKeys,
  valuesIndex,
  enCode,
  deCode,
} = require('../lib/util');

const keys = require('../lib/keys');

const USER_TABLE_NAME = 'users';

// CRUD COMMON
const insertOneCommon = commonSvc.insertOne(
  getStringKeys,
  valuesIndex,
  USER_TABLE_NAME
);

const findOneCommon = commonSvc.findOne(fConditions, USER_TABLE_NAME);

// ENCODE AND DECODE COMMON
const enCodeCommon = commonSvc.crypto(enCode)(keys.publicKey);
const deCodeCommon = commonSvc.crypto(deCode)(keys.privateKey);

// USER SERVICE
const insertUser = data => insertOneCommon(data);
const findUser = conditions => findOneCommon(conditions);
const hashPaswword = password => Promise.resolve(enCodeCommon(password));
const deHashPassword = password => Promise.resolve(deCodeCommon(password));

module.exports = {
  deHashPassword,
  hashPaswword,
  insertUser,
  findUser,
};
