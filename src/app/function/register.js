const R = require('ramda');
const createError = require('http-errors');
const userSvc = require('../../service/user.service');
const serviceCaller = require('../../lib/serviceCaller');

module.exports = async ctx => {
  try {
    const { name, email, password } = ctx.request.body;

    const deHashPassword = await userSvc.deHashPassword(password);

    const hashPawd = await userSvc.hashPaswword(deHashPassword);

    const uuid = await serviceCaller.getUUID();

    const { rows } = await userSvc.insertUser({
      id: uuid,
      name,
      email,
      password: hashPawd,
    });

    const accessToken = await serviceCaller.generateJWT(
      R.pick(
        [
          'id',
          'email',
          'name',
          'username',
          'email_verified',
          'created_at',
          'updated_at',
        ],
        rows[0]
      )
    );

    ctx.status = 200;
    ctx.body = {
      users: R.omit(
        ['password', 'soft_delete', 'verify_token', 'reset_token'],
        rows[0]
      ),
      access_token: accessToken,
    };
  } catch (error) {
    ctx.throw(
      error.code === '23505'
        ? createError(422, 'unique_violation', {
            errors: [
              {
                code: 422,
                message: error.detail,
              },
            ],
          })
        : error
    );
  }
};
