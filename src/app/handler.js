const register = require('./function/register');
const login = require('./function/login');
const hashPaswword = require('./function/hashPaswword');

module.exports = {
  login,
  register,
  hashPaswword,
};
