const register = {
  type: 'object',
  required: ['name', 'email', 'password'],
  properties: {
    name: { type: 'string' },
    email: { type: 'string', format: 'email' },
    password: { type: 'string' },
  },
};

const login = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: { type: 'string', format: 'email' },
    password: { type: 'string' },
  },
};

const hashPaswword = {
  type: 'object',
  required: ['password'],
  properties: {
    password: { type: 'string' },
  },
};

module.exports = {
  login,
  register,
  hashPaswword,
};
