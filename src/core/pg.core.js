const { Pool, Client } = require('pg');

let pool;

const connect = pgUrl =>
  new Promise(async (resolve, reject) => {
    try {
      resolve(
        new Pool({
          connectionString: pgUrl,
        })
      );
    } catch (error) {
      reject(error);
    }
  });

module.exports = {
  query: query => pool.query(query),
  client: () => pool.connect(),
  connect,
};
