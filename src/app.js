const http = require('http');
const config = require('config');
const router = require('./app/router');
const logger = require('./core/logger');
const app = require('./core/koa.core')(router, logger);
const pg = require('./core/pg.core');

// const startApiServer = () =>
const startApiServer = () =>
  new Promise((resolve, reject) => {
    http.createServer(app.callback()).listen(config.get('port'), err => {
      if (err) return reject(err);
      return resolve();
    });
  });

const start = async () => {
  try {
    await pg.connect(config.get('pgUrl'));
    logger.info(`[POSTGRES] connected to ${config.get('pgUrl')}`);

    await startApiServer();

    logger.info(
      `${'[MAIN]'} Server is listening on port ${config.get('port')}`
    );
  } catch (error) {
    logger.error(error);
    process.exit(1);
  }
};

start();

const shutdown = signal => async err => {
  logger.log(`${signal}...`);
  if (err) logger.error(err.stack || err);
};

process.on('SIGTERM', shutdown('SIGNTERM'));
