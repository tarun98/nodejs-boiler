FROM node:12.12.0

WORKDIR /opt/app
ENV NODE_ENV=development \
    PORT=8000 \
    HOST=localhost  
    
COPY package.json /opt/app
COPY package-lock.json /opt/app

RUN npm install

COPY . /home/app

EXPOSE 8000

CMD ["node", "./src/app.js"]


